/**    
 * 文件名：LinkMessage.java    
 * 版本信息：    
 * 日期：2014-5-29    
 * Copyright  Corporation 2014     
 * 版权所有    
 */
package sbl.qp.bean.request;

/**    
 * 项目名称：sblqp    
 * 类名称：LinkMessage    
 * 类描述： 请求消息之链接消息   
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午4:48:31    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午4:48:31    
 * 修改备注：    
 * @version     
 */
public class LinkMessage extends BaseMessage {  
    // 消息标题  
    private String Title;  
    // 消息描述  
    private String Description;  
    // 消息链接  
    private String Url;  
  
    public String getTitle() {  
        return Title;  
    }  
  
    public void setTitle(String title) {  
        Title = title;  
    }  
  
    public String getDescription() {  
        return Description;  
    }  
  
    public void setDescription(String description) {  
        Description = description;  
    }  
  
    public String getUrl() {  
        return Url;  
    }  
  
    public void setUrl(String url) {  
        Url = url;  
    }  
} 
