package sbl.qp.bean.request;

/**
 * 项目名称：sblqp    
 * 类名称：ImageMessage    
 * 类描述：    图片消息
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午4:45:23    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午4:45:23    
 * 修改备注：    
 * @version     
 */
public class ImageMessage extends BaseMessage {  
    // 图片链接  
    private String PicUrl;  
  
    public String getPicUrl() {  
        return PicUrl;  
    }  
  
    public void setPicUrl(String picUrl) {  
        PicUrl = picUrl;  
    }  
}  