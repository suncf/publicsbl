/**    
 * 文件名：VoiceMessage.java    
 * 版本信息：    
 * 日期：2014-5-29    
 * Copyright  Corporation 2014     
 * 版权所有    
 */
package sbl.qp.bean.request;

/**    
 * 项目名称：sblqp    
 * 类名称：VoiceMessage    
 * 类描述：  音频消息 
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午4:48:59    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午4:48:59    
 * 修改备注：    
 * @version     
 */
public class VoiceMessage {  
    // 媒体ID  
    private String MediaId;  
    // 语音格式  
    private String Format;  
  
    public String getMediaId() {  
        return MediaId;  
    }  
  
    public void setMediaId(String mediaId) {  
        MediaId = mediaId;  
    }  
  
    public String getFormat() {  
        return Format;  
    }  
  
    public void setFormat(String format) {  
        Format = format;  
    }  
}  
