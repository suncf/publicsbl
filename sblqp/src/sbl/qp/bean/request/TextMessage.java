package sbl.qp.bean.request;

/**
 * 项目名称：sblqp    
 * 类名称：TextMessage    
 * 类描述：文本消息
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午4:45:57    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午4:45:57    
 * 修改备注：    
 * @version     
 */
public class TextMessage extends BaseMessage {  
    // 消息内容  
    private String Content;  
  
    public String getContent() {  
        return Content;  
    }  
  
    public void setContent(String content) {  
        Content = content;  
    }  
}  