/**    
 * 文件名：TextMessage.java    
 * 版本信息：    
 * 日期：2014-5-29    
 * Copyright  Corporation 2014     
 * 版权所有    
 */
package sbl.qp.bean.response;

/**    
 * 项目名称：sblqp    
 * 类名称：TextMessage    
 * 类描述：   文本消息 
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午4:53:58    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午4:53:58    
 * 修改备注：    
 * @version     
 */
public class TextMessage extends BaseMessage {  
    // 回复的消息内容  
    private String Content;  
    public String getContent() {  
        return Content;  
    }  
    public void setContent(String content) {  
        Content = content;  
    }  
}  
