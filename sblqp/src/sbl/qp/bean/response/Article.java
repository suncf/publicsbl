/**    
 * 文件名：Article.java    
 * 版本信息：    
 * 日期：2014-5-29    
 * Copyright  Corporation 2014     
 * 版权所有    
 */
package sbl.qp.bean.response;

/**    
 * 项目名称：sblqp    
 * 类名称：Article    
 * 类描述：  图文基类  
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午4:56:57    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午4:56:57    
 * 修改备注：    
 * @version     
 */
public class Article {
	// 图文消息名称  
    private String Title;  
    // 图文消息描述  
    private String Description;  
    // 图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80，限制图片链接的域名需要与开发者填写的基本资料中的Url一致  
    private String PicUrl;  
    // 点击图文消息跳转链接  
    private String Url;  
  
    public String getTitle() {  
        return Title;  
    }  
  
    public void setTitle(String title) {  
        Title = title;  
    }  
  
    public String getDescription() {  
        return null == Description ? "" : Description;  
    }  
  
    public void setDescription(String description) {  
        Description = description;  
    }  
  
    public String getPicUrl() {  
        return null == PicUrl ? "" : PicUrl;  
    }  
  
    public void setPicUrl(String picUrl) {  
        PicUrl = picUrl;  
    }  
  
    public String getUrl() {  
        return null == Url ? "" : Url;  
    }  
  
    public void setUrl(String url) {  
        Url = url;  
    }
}
