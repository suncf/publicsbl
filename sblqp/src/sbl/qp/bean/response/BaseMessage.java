/**    
 * 文件名：BaseMessage.java    
 * 版本信息：    
 * 日期：2014-5-29    
 * Copyright  Corporation 2014     
 * 版权所有    
 */
package sbl.qp.bean.response;

/**    
 * 项目名称：sblqp    
 * 类名称：BaseMessage    
 * 类描述：响应消息的基类 消息基类（公众帐号 -> 普通用户）     
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午4:52:30    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午4:52:30    
 * 修改备注：    
 * @version     
 */
public class BaseMessage {
	// 接收方帐号（收到的OpenID）  
    private String ToUserName;  
    // 开发者微信号  
    private String FromUserName;  
    // 消息创建时间 （整型）  
    private long CreateTime;  
    // 消息类型（text/music/news）  
    private String MsgType;  
    // 位0x0001被标志时，星标刚收到的消息  
    private int FuncFlag;  
  
    public String getToUserName() {  
        return ToUserName;  
    }  
  
    public void setToUserName(String toUserName) {  
        ToUserName = toUserName;  
    }  
  
    public String getFromUserName() {  
        return FromUserName;  
    }  
  
    public void setFromUserName(String fromUserName) {  
        FromUserName = fromUserName;  
    }  
  
    public long getCreateTime() {  
        return CreateTime;  
    }  
  
    public void setCreateTime(long createTime) {  
        CreateTime = createTime;  
    }  
  
    public String getMsgType() {  
        return MsgType;  
    }  
  
    public void setMsgType(String msgType) {  
        MsgType = msgType;  
    }  
  
    public int getFuncFlag() {  
        return FuncFlag;  
    }  
  
    public void setFuncFlag(int funcFlag) {  
        FuncFlag = funcFlag;  
    } 
}
