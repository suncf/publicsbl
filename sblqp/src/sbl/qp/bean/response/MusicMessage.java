/**    
 * 文件名：MusicMessage.java    
 * 版本信息：    
 * 日期：2014-5-29    
 * Copyright  Corporation 2014     
 * 版权所有    
 */
package sbl.qp.bean.response;

/**    
 * 项目名称：sblqp    
 * 类名称：MusicMessage    
 * 类描述：响应消息之音乐消息    
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午4:54:53    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午4:54:53    
 * 修改备注：    
 * @version     
 */
public class MusicMessage extends BaseMessage {  
    // 音乐  
    private Music Music;  
  
    public Music getMusic() {  
        return Music;  
    }  
  
    public void setMusic(Music music) {  
        Music = music;  
    }  
}  
