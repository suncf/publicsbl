/**    
 * 文件名：NewsMessage.java    
 * 版本信息：    
 * 日期：2014-5-29    
 * Copyright  Corporation 2014     
 * 版权所有    
 */
package sbl.qp.bean.response;

import java.util.List;

/**    
 * 项目名称：sblqp    
 * 类名称：NewsMessage    
 * 类描述： 响应消息->图文
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午4:56:17    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午4:56:17    
 * 修改备注：    
 * @version     
 */
public class NewsMessage extends BaseMessage {  
    // 图文消息个数，限制为10条以内  
    private int ArticleCount;  
    // 多条图文消息信息，默认第一个item为大图  
    private List<Article> Articles;  
  
    public int getArticleCount() {  
        return ArticleCount;  
    }  
  
    public void setArticleCount(int articleCount) {  
        ArticleCount = articleCount;  
    }  
  
    public List<Article> getArticles() {  
        return Articles;  
    }  
  
    public void setArticles(List<Article> articles) {  
        Articles = articles;  
    }  
}
