/**    
 * 文件名：CoreService.java    
 * 版本信息：    
 * 日期：2014-5-29    
 * Copyright  Corporation 2014     
 * 版权所有    
 */
package sbl.qp.core;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import sbl.qp.bean.response.TextMessage;
import sbl.qp.tools.MessageUtil;

/**    
 * 项目名称：sblqp    
 * 类名称：CoreService    
 * 类描述：服务核心类    
 * 创建人：suncf    
 * 创建时间：2014-5-29 下午5:14:08    
 * 修改人：suncf    
 * 修改时间：2014-5-29 下午5:14:08    
 * 修改备注：    
 * @version     
 */
public class CoreService {
	
	public static String processRequest(HttpServletRequest request) {
		String respMessage = null;
		try {
			// 默认返回的文本消息内容
			String respContent = "请求处理异常，请稍候尝试！";
			// xml请求解析
			Map<String, String> requestMap = MessageUtil.parseXml(request);

			// 发送方帐号（open_id）
			String fromUserName = requestMap.get("FromUserName");
			// 公众帐号
			String toUserName = requestMap.get("ToUserName");
			// 消息类型
			String msgType = requestMap.get("MsgType");

			 //文本消息
            String msgContent = requestMap.get("Content");  
            // 图片信息
            String picUrl = requestMap.get("PicUrl");  
            String mediaId = requestMap.get("MediaId");  
            // 地理位置信息
            String local=requestMap.get("Label");  
            
            
			// 回复文本消息
			TextMessage textMessage = new TextMessage();
			textMessage.setToUserName(fromUserName);
			textMessage.setFromUserName(toUserName);
			textMessage.setCreateTime(new Date().getTime());
			textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
			textMessage.setFuncFlag(0);

			// 文本消息
			if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_TEXT)) {
				//respContent = "您发送的是文本消息！";
				if("?".equals(msgContent) || "Help Me".equalsIgnoreCase(msgContent) || "help".equalsIgnoreCase(msgContent)){
					respContent = getHelpMenu();
				} else if("1".equals(msgContent)){
					StringBuffer compInfo = new StringBuffer();
					compInfo.append("哈尔滨486汽配商行主营斯巴鲁配件,");
					compInfo.append("公司以优质的服务").append("\n");
					compInfo.append("品质保证").append("\n");
					compInfo.append("价格最低\n受到广大客户的一致好评!").append("\n");
					compInfo.append("真诚欢迎各界客户来洽谈贸易").append("\n");
					compInfo.append("合作经营/:share").append("\n");
					compInfo.append("共创辉煌/::>").append("\n");
					respContent = compInfo.toString();
				} else if("2".equals(msgContent)){
					StringBuffer addrInfo = new StringBuffer();
					addrInfo.append("公司位于黑龙江省哈尔滨市宣化街105号,");
					addrInfo.append("交通十分便捷,可乘坐公交路线有:").append("\n");
					addrInfo.append("3、7、8、17、25、30、60").append("\n");
					addrInfo.append("63、72、84、86、93、112").append("\n");
					addrInfo.append("宣化街下车即是/:,@-D").append("\n");
					respContent = addrInfo.toString();
				} else if("3".equals(msgContent)){
					StringBuffer bandInfo = new StringBuffer();
					bandInfo.append("哈尔滨486汽配商行主营斯巴鲁配件,");
					bandInfo.append("其中品牌包括:").append("\n");
					bandInfo.append("森林人\n傲虎\n力狮\n");
					bandInfo.append("翼豹\n驰鹏\nXV配件等").append("\n");
					bandInfo.append("真诚欢迎各界客户来洽谈贸易！/:share");
					respContent = bandInfo.toString();
				} else if("4".equals(msgContent)){
					// 联系方式
					StringBuffer contact = new StringBuffer();
					contact.append("地址:哈尔滨市宣化街105号").append("\n");
					contact.append("电话:0451-83103486").append("\n");
					contact.append("传真:0451-86134252").append("\n");
					contact.append("手机:13895748699").append("\n");
					contact.append("Email:1550632789@qq.com").append("\n");
					respContent = contact.toString();
				} else {
					respContent = getHelpMenu();
				}
			}
			// 图片消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_IMAGE)) {
				respContent = getHelpMenu();
			}
			// 地理位置消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {
				//respContent = "您发送的是地理位置消息！";
				respContent = getHelpMenu();
			}
			// 链接消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LINK)) {
				//respContent = "您发送的是链接消息！";
				respContent = getHelpMenu();
			}
			// 音频消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VOICE)) {
				//respContent = "您发送的是音频消息！";
				respContent = getHelpMenu();
			}
			// 事件推送
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_EVENT)) {
				// 事件类型
				String eventType = requestMap.get("Event");
				// 订阅
				if (eventType.equals(MessageUtil.EVENT_TYPE_SUBSCRIBE)) {
					StringBuffer message = new StringBuffer();
					message.append("您好，欢迎关注哈尔滨468汽配商行微信账号！").append("\n");
					message.append("回复?,显示帮助菜单,或大叫Help Me/::S");
					respContent = message.toString();
					
				}
				// 取消订阅
				else if (eventType.equals(MessageUtil.EVENT_TYPE_UNSUBSCRIBE)) {
					// TODO 取消订阅后用户再收不到公众号发送的消息，因此不需要回复消息
				}
				// 自定义菜单点击事件
				else if (eventType.equals(MessageUtil.EVENT_TYPE_CLICK)) {
					// TODO 自定义菜单权没有开放，暂不处理该类消息
				}
			}
			textMessage.setContent(respContent);
			respMessage = MessageUtil.textMessageToXml(textMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return respMessage;
	}
	
	/**
	 * getHelpMenu(这里用一句话描述这个方法的作用)    
	 * TODO(这里描述这个方法适用条件 – 可选)    
	 * TODO(这里描述这个方法的执行流程 – 可选)     
	 * TODO(这里描述这个方法的使用方法 – 可选)    
	 * TODO(这里描述这个方法的注意事项 – 可选)    
	 * @param   name       
	 * @return String   帮助菜单 
	 * @Exception 异常对象    
	 * @since  CodingExample　Ver(编码范例查看) 1.1
	 */
	public static String getHelpMenu(){
		StringBuffer message = new StringBuffer();
		message.append("您好,请回复以下数字,选择相应服务类型!").append("\n");
		message.append("1.公司介绍").append("\n");
		message.append("2.公司地址").append("\n");
		message.append("3.主营品牌").append("\n");
		message.append("4.联系我们").append("\n");
		message.append("小提示:也可以直接输入您要咨询的商品哦~~!").append("\n");
		return message.toString();
	}
}
